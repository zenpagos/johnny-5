CLI_NAME = 'Zenpagos'

DEV_ENV_KEY = 'dev'
PROD_ENV_KEY = 'prod'
VALID_ENVS = [
    DEV_ENV_KEY,
    PROD_ENV_KEY,
]

PARAMETER_PATH = 'parameters.yaml'
