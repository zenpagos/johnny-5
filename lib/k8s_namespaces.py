import click

from .commons import create_k8s_resource_from_manifest_file, delete_k8s_resource_from_manifest_file
from .load_parameters import load_k8s_manifests


def create_namespaces(env):
    click.secho('Creating namespaces', fg='cyan')

    namespaces = load_k8s_manifests(env).get('namespaces')
    for namespace in namespaces:
        create_k8s_resource_from_manifest_file(namespace)


def delete_namespaces(env):
    click.secho('Deleting namespaces', fg='cyan')

    namespaces = load_k8s_manifests(env).get('namespaces')
    for namespace in namespaces:
        delete_k8s_resource_from_manifest_file(file=namespace, raise_exception=False)
