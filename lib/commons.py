import subprocess

import click
import emoji


def _add_namespace(cmd, namespace):
    """ Add namespace to a command """
    if namespace:
        cmd.append('--namespace')
        cmd.append(namespace)

    return cmd


def _add_values_file(cmd, values_file):
    """ Add helm values file to a command """
    if values_file:
        cmd.append('-f')
        cmd.append(values_file)

    return cmd


def _execute_process(cmd):
    """ Execute a process """
    return subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)


def get_executed_command(proc):
    return ' '.join(map(str, proc.args))


def install_chart(deployment_name, chart, values_file=None, namespace=None, raise_exception=True):
    """ Install a Helm chart """
    cmd = ['helm', 'upgrade', '--install', deployment_name, chart]
    cmd = _add_values_file(cmd=cmd, values_file=values_file)
    cmd = _add_namespace(cmd=cmd, namespace=namespace)

    out = _execute_process(cmd)
    _, err = out.communicate()

    if err:
        err_str = err.decode('utf-8')
        command_str = get_executed_command(out)
        click.secho(emoji.emojize(f':fire: Error installing "{deployment_name}" chart'), fg='red')
        click.secho(f'Executed command: {command_str}')
        click.secho(f'Error: {err_str}', fg='red')

        if raise_exception:
            raise click.Abort

    click.secho(emoji.emojize(f':heavy_check_mark: Chart "{deployment_name}" installed successfully'), fg='green')


def uninstall_chart(deployment_name, namespace=None, raise_exception=True):
    """ Uninstall a Helm chart """
    cmd = ['helm', 'uninstall', deployment_name]
    cmd = _add_namespace(cmd=cmd, namespace=namespace)

    out = _execute_process(cmd)
    _, err = out.communicate()

    if err:
        err_str = err.decode('utf-8')
        command_str = get_executed_command(out)
        click.secho(emoji.emojize(f':fire: Error uninstalling "{deployment_name}" chart'), fg='red')
        click.secho(f'Executed command: {command_str}')
        click.secho(f'Error: {err_str}', fg='red')

        if raise_exception:
            raise click.Abort

    click.secho(emoji.emojize(f':heavy_check_mark: Chart "{deployment_name}" uninstalled successfully'), fg='green')


def delete_k8s_secret(secret_name, raise_exception=True):
    """ Delete a k8s secret """
    cmd = ['kubectl', 'delete', 'secret', secret_name]

    out = _execute_process(cmd)
    _, err = out.communicate()

    if err:
        err_str = err.decode('utf-8')
        command_str = get_executed_command(out)
        click.secho(emoji.emojize(f':fire: Error deleting secret "{secret_name}"'), fg='red')
        click.secho(f'Executed command: {command_str}')
        click.secho(f'Error: {err_str}', fg='red')

        if raise_exception:
            raise click.Abort

    click.secho(emoji.emojize(f':heavy_check_mark: Secret "{secret_name}" deleted successfully'), fg='green')


def create_k8s_resource_from_manifest_file(file, raise_exception=True):
    """ Create a kubernetes resource from a manifest file """
    cmd = ['kubectl', 'apply', '--validate=false', '-f', file]

    out = _execute_process(cmd)
    _, err = out.communicate()

    if err:
        err_str = err.decode('utf-8')
        command_str = get_executed_command(out)
        click.secho(emoji.emojize(f':fire: Error creating resource "{file}"'), fg='red')
        click.secho(f'Executed command: {command_str}')
        click.secho(f'Error: {err_str}', fg='red')

        if raise_exception:
            raise click.Abort

    click.secho(emoji.emojize(f':heavy_check_mark: Resource "{file}" created successfully'), fg='green')


def delete_k8s_resource_from_manifest_file(file, raise_exception=True):
    """ Delete a kubernetes resource from a manifest file """
    cmd = ['kubectl', 'delete', '-f', file]

    out = _execute_process(cmd)
    _, err = out.communicate()

    if err:
        err_str = err.decode('utf-8')
        command_str = get_executed_command(out)
        click.secho(emoji.emojize(f':fire: Error deleting resource "{file}"'), fg='red')
        click.secho(f'Executed command: {command_str}')
        click.secho(f'Error: {err_str}', fg='red')

        if raise_exception:
            raise click.Abort

    click.secho(emoji.emojize(f':heavy_check_mark: Resource "{file}" deleted successfully'), fg='green')


def add_helm_repository(name, url, raise_exception=True):
    """ Add helm a repository """
    cmd = ['helm', 'repo', 'add', name, url]

    out = _execute_process(cmd)
    _, err = out.communicate()

    if err:
        err_str = err.decode('utf-8')
        command_str = get_executed_command(out)
        click.secho(emoji.emojize(f':fire: Error adding repository "{name}"'), fg='red')
        click.secho(f'Executed command: {command_str}')
        click.secho(f'Error: {err_str}', fg='red')

        if raise_exception:
            raise click.Abort

    click.secho(emoji.emojize(f':heavy_check_mark: Repository "{name}" added successfully'), fg='green')


def remove_helm_repository(name, raise_exception=True):
    """ Remove helm a repository """
    cmd = ['helm', 'repo', 'remove', name]

    out = _execute_process(cmd)
    _, err = out.communicate()

    if err:
        err_str = err.decode('utf-8')
        command_str = get_executed_command(out)
        click.secho(emoji.emojize(f':fire: Error removing repository "{name}"'), fg='red')
        click.secho(f'Executed command: {command_str}')
        click.secho(f'Error: {err_str}', fg='red')

        if raise_exception:
            raise click.Abort

    click.secho(emoji.emojize(f':heavy_check_mark: Repository "{name}" removed successfully'), fg='green')
