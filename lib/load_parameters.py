import yaml

from .constants import PARAMETER_PATH, VALID_ENVS


def _load_parameters():
    """ Load parameters from parameters.yaml file """
    try:
        stream = open(PARAMETER_PATH, 'r')
        return yaml.load(stream=stream,
                         Loader=yaml.FullLoader)
    except yaml.YAMLError as exc:
        print("Error in configuration file:", exc)


def _validate_env(env):
    if env not in VALID_ENVS:
        raise ValueError('environment not valid')


def load_programs():
    return _load_parameters().get('programs')


def load_k8s_manifests(env):
    _validate_env(env)
    return _load_parameters().get('k8sManifests').get(env)


def load_helm_repositories():
    return _load_parameters().get('helmRepositories')


def load_kong_configuration(env):
    _validate_env(env)
    return _load_parameters().get('helmCharts').get(env).get('kong')


def load_cert_manager_configuration(env):
    _validate_env(env)
    return _load_parameters().get('helmCharts').get(env).get('certManager')


def load_mysql_configuration(env):
    _validate_env(env)
    return _load_parameters().get('helmCharts').get(env).get('mysql')


def load_kafka_configuration(env):
    _validate_env(env)
    return _load_parameters().get('helmCharts').get(env).get('kafka')
