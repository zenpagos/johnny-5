import click

from .commons import install_chart, uninstall_chart
from .load_parameters import load_kong_configuration


def install_kong(env):
    click.secho('Installing Kong', fg='cyan')

    kong_conf = load_kong_configuration(env)
    deployment_name = kong_conf.get('deploymentName')
    chart = kong_conf.get('chart')
    values_file = kong_conf.get('valuesFile')

    install_chart(deployment_name=deployment_name, chart=chart, values_file=values_file)


def uninstall_kong(env):
    click.secho('Uninstalling Kong', fg='cyan')

    kong_conf = load_kong_configuration(env)
    deployment_name = kong_conf.get('deploymentName')

    uninstall_chart(deployment_name=deployment_name, raise_exception=False)
