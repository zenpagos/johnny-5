import subprocess
import time

import click
import emoji

from .commons import install_chart, uninstall_chart, delete_k8s_secret
from .load_parameters import load_kafka_configuration


def _create_secret(secret_name, host, port):
    secret_man_out = subprocess.Popen(
        [
            'kubectl', 'create', 'secret', 'generic', secret_name,
            f'--from-literal=host={host}',
            f'--from-literal=port={port}',
            '--dry-run', '-o', 'yaml'
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    create_secret_out = subprocess.Popen(
        ['kubectl', 'apply', '-f', '-'],
        stdin=secret_man_out.stdout,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    out, err = create_secret_out.communicate()
    if err:
        click.secho(emoji.emojize(f':fire: Error creating secret'), fg='red')
        click.secho(err.decode('utf-8'), fg='red')
        raise click.Abort
    click.secho(emoji.emojize(f':heavy_check_mark: Secret "{secret_name}" created successfully'), fg='green')


def _get_pod_name(deployment_name):
    get_pod_name = subprocess.Popen(
        [
            'kubectl', 'get', 'pods',
            '-l', f'app.kubernetes.io/name=kafka,app.kubernetes.io/instance={deployment_name}',
            '-o', 'jsonpath="{.items[0].metadata.name}"'
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    out, err = get_pod_name.communicate()
    if err:
        click.secho(emoji.emojize(f':fire: Error getting pod name'), fg='red')
        click.secho(err.decode('utf-8'), fg='red')
        raise click.Abort

    return str(out, 'utf-8').replace('"', '')


def _get_pod_status(pod_name):
    get_pod_status = subprocess.Popen(
        ['kubectl', 'get', 'pods', pod_name, '-o', 'jsonpath={..status.conditions[?(@.type=="Ready")].status}'],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    out, err = get_pod_status.communicate()
    if err:
        click.secho(emoji.emojize(f':fire: Error getting pod status for pod "{pod_name}"'), fg='red')
        click.secho(err.decode('utf-8'), fg='red')
        raise click.Abort

    return str(out, 'utf-8')


def _create_topics(pod_name, topics):
    while not bool(_get_pod_status(pod_name)):
        time.sleep(1)

    for topic in topics:
        get_pod_status = subprocess.Popen(
            [
                'kubectl', 'exec', '-it', pod_name, '--', 'kafka-topics.sh',
                '--zookeeper', 'queue-zookeeper:2181', '--create',
                '--partitions', '6', '--replication-factor', '1', '--topic', topic
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        get_pod_status.communicate()
        click.secho(emoji.emojize(f':heavy_check_mark: Topic {topic} created successfully'), fg='green')


def install_kafka(env):
    click.secho('Installing Kafka', fg='cyan')

    kafka_conf = load_kafka_configuration(env)
    deployment_name = kafka_conf.get('deploymentName')
    chart = kafka_conf.get('chart')
    values_file = kafka_conf.get('valuesFile')
    secret_name = kafka_conf.get('secretName')
    topics = kafka_conf.get('topics')
    host = f'{deployment_name}-kafka'
    port = 9092

    install_chart(deployment_name=deployment_name, chart=chart, values_file=values_file)

    _create_secret(secret_name=secret_name, host=host, port=port)

    pod_name = _get_pod_name(deployment_name)
    _create_topics(pod_name=pod_name, topics=topics)


def uninstall_kafka(env):
    click.secho('Uninstalling Kafka', fg='cyan')

    kafka_conf = load_kafka_configuration(env)
    deployment_name = kafka_conf.get('deploymentName')
    secret_name = kafka_conf.get('secretName')

    uninstall_chart(deployment_name=deployment_name, raise_exception=False)
    delete_k8s_secret(secret_name=secret_name, raise_exception=False)
