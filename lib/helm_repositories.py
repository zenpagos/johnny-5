import click

from .commons import add_helm_repository, remove_helm_repository
from .load_parameters import load_helm_repositories


def add_helm_repositories():
    click.secho('Adding Helm Repositories', fg='cyan')

    for repository in load_helm_repositories():
        name = repository['name']
        url = repository['url']

        add_helm_repository(name=name, url=url)


def remove_helm_repositories():
    click.secho('Removing Helm Repositories', fg='cyan')

    for repository in load_helm_repositories():
        name = repository['name']

        remove_helm_repository(name=name, raise_exception=False)
