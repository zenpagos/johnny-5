import click

from .commons import (
    install_chart,
    uninstall_chart,
    delete_k8s_secret,
    create_k8s_resource_from_manifest_file,
    delete_k8s_resource_from_manifest_file
)
from .load_parameters import load_cert_manager_configuration


def install_cert_manager(env):
    click.secho('Installing Cert Manager', fg='cyan')

    cert_manager_conf = load_cert_manager_configuration(env)

    custom_resources = cert_manager_conf.get('customResources')
    for resource in custom_resources:
        create_k8s_resource_from_manifest_file(resource)

    deployment_name = cert_manager_conf.get('deploymentName')
    chart = cert_manager_conf.get('chart')
    namespace = cert_manager_conf.get('namespace')

    install_chart(deployment_name=deployment_name, chart=chart, namespace=namespace)


def uninstall_cert_manager(env):
    click.secho('Uninstalling Cert Manager', fg='cyan')

    cert_manager_conf = load_cert_manager_configuration(env)

    custom_resources = cert_manager_conf.get('customResources')
    for resource in custom_resources:
        delete_k8s_resource_from_manifest_file(file=resource, raise_exception=False)

    deployment_name = cert_manager_conf.get('deploymentName')
    namespace = cert_manager_conf.get('namespace')

    uninstall_chart(deployment_name=deployment_name, namespace=namespace, raise_exception=False)

    delete_k8s_secret(secret_name='zenpagos-cert-secret', raise_exception=False)
