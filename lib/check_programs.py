from shutil import which

import click
import emoji

from .load_parameters import load_programs


def check_programs():
    click.secho('Checking required programs', fg='cyan')

    for program in load_programs():
        if which(program) is None:
            click.secho(emoji.emojize(f':fire: {program} is not installed'), fg='red')
            raise click.Abort

        click.secho(emoji.emojize(f':heavy_check_mark: {program}'), fg='green')
