import base64
import subprocess

import click
import emoji

from .commons import install_chart, uninstall_chart, delete_k8s_secret
from .load_parameters import load_mysql_configuration


def _get_password(host):
    get_password_out = subprocess.Popen(
        ['kubectl', 'get', 'secret', host, '-o', 'jsonpath="{.data.mysql-root-password}"'],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    pass_out, err = get_password_out.communicate()
    if err:
        click.secho(emoji.emojize(f':fire: Error getting MySQL password'), fg='red')
        click.secho(err.decode('utf-8'), fg='red')
        raise click.Abort

    return str(base64.decodebytes(pass_out), 'utf-8')


def _create_secret(secret_name, host, username, password):
    secret_man_out = subprocess.Popen(
        [
            'kubectl', 'create', 'secret', 'generic', secret_name,
            f'--from-literal=host={host}',
            f'--from-literal=username={username}',
            f'--from-literal=password={password}',
            '--dry-run', '-o', 'yaml'
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    create_secret_out = subprocess.Popen(
        ['kubectl', 'apply', '-f', '-'],
        stdin=secret_man_out.stdout,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    out, err = create_secret_out.communicate()
    if err:
        click.secho(emoji.emojize(f':fire: Error creating secret'), fg='red')
        click.secho(err.decode('utf-8'), fg='red')
        raise click.Abort
    click.secho(emoji.emojize(f':heavy_check_mark: Secret "{secret_name}" created successfully'), fg='green')


def install_mysql(env):
    click.secho('Installing Mysql', fg='cyan')

    mysql_conf = load_mysql_configuration(env)
    deployment_name = mysql_conf.get('deploymentName')
    chart = mysql_conf.get('chart')
    values_file = mysql_conf.get('valuesFile')
    secret_name = mysql_conf.get('secretName')
    host = f'{deployment_name}-mysql'
    username = 'root'

    install_chart(deployment_name=deployment_name, chart=chart, values_file=values_file)

    password = _get_password(host)

    _create_secret(secret_name=secret_name, host=host, username=username, password=password)


def uninstall_mysql(env):
    click.secho('Uninstalling Mysql', fg='cyan')

    mysql_conf = load_mysql_configuration(env)
    deployment_name = mysql_conf.get('deploymentName')
    secret_name = mysql_conf.get('secretName')

    uninstall_chart(deployment_name=deployment_name, raise_exception=False)
    delete_k8s_secret(secret_name=secret_name, raise_exception=False)
