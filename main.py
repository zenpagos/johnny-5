import click

from lib.cert_manager import install_cert_manager, uninstall_cert_manager
from lib.check_programs import check_programs
from lib.helm_repositories import add_helm_repositories, remove_helm_repositories
from lib.k8s_namespaces import create_namespaces, delete_namespaces
from lib.kong import install_kong, uninstall_kong
from lib.mysql import install_mysql, uninstall_mysql
from lib.splash import print_splash
from lib.constants import VALID_ENVS
from lib.kafka import install_kafka, uninstall_kafka

__author__ = "Jesus Diaz"


@click.group()
def main():
    """
    Zenpagos deployment tool
    """
    pass


@main.command()
@click.option('--env', required=True, type=click.Choice(VALID_ENVS))
def setup(env):
    print_splash()
    check_programs()
    add_helm_repositories()
    create_namespaces(env)
    install_kong(env)
    install_mysql(env)
    install_kafka(env)
    install_cert_manager(env)


@main.command()
@click.option('--env', required=True, type=click.Choice(VALID_ENVS))
def cleanup(env):
    print_splash()
    check_programs()
    uninstall_cert_manager(env)
    uninstall_kong(env)
    uninstall_mysql(env)
    uninstall_kafka(env)
    delete_namespaces(env)
    remove_helm_repositories()


if __name__ == "__main__":
    main()
